<style>
.home-pageslidercontainer-middle img{
 display: block !important;
max-width: 100% !important;
/*height: 374px !important;*/
text-align: center;
margin: auto;
 
 }
body.home .site-main .container.background-shadow{background:none;box-shadow:none;}
	.home-pageslider{

	background: #fff;
	-moz-box-shadow: 0 0 20px -10px #000;
	-ms-box-shadow: 0 0 20px -10px #000;
	-o-box-shadow: 0 0 20px -10px #000;
	-webkit-box-shadow: 0 0 20px -10px #000;
	box-shadow: 0 0 20px -10px #000;
	float: left;
	margin-top: 20px;
	position: relative;
	width: 100%;
margin-bottom:20px;
padding:30px 0 !important;
}
.clear{clear:both;}
.left-wrapper{
 clear: left;
    float: left;
    padding: 20px;
    width: 200px;
}
.right-wrapper{
   clear: right;
    float: right;
    padding: 20px 20px 18px 20px;
    width: 200px;
}
.home-pageslidercontainer{
	float: left;
	width: 760px;
}
.home-pageslidercontainer-middle{
	border-left: 2px dotted #c3c3c3;
	border-right: 2px dotted #c3c3c3;
	float: right;
	width: 490px;
	position: relative;
	height: 374px;
	
}
.home-pageslidercontainer-left{
	float: left;
	
	width: 200px;
	clear:left;
	
}

.home-page-slider-right{
	float: right;

width: 200px;
clear:right;

}
.main-wrapper{
	float:left;
}
.home-pageslider a, .home-pageslider a:visited{
	text-align: center;
	width: 100%;
	display: block;
	color: #000;
	font-weight: 600;
}

.home-pageslider a:hover{
	-moz-transition: color 0.2s;
	-ms-transition: color 0.2s;
	-o-transition: color 0.2s;
	-webkit-transition: color 0.2s;
	transition: color 0.2s;
	color:#f00a71;
	text-decoration: none;
}
.home-pageslidercontainer-middle .middle-block-content{
	padding: 0 15px;
	position: absolute;
	width: 88%;
	text-align: center;
	top: 121px;
	background-color: #fff;
	left: 3%;
}
.middle-title{
	font-size:30px;
	line-height: 140%
}
.right-left-title{
	// font-size:1.12em;
	font-size: 15px;
	line-height: 140%;
}
a.more-link{
font-weight:normal;
}
@media screen and (max-width: 1024px) {
.home-pageslidercontainer-middle .middle-block-content{padding:0;
}
.main-wrapper {
    width: 68%;
}
.home-pageslidercontainer-middle {
    width: 49%;
	 height: 513px;
}


.left-wrapper {
    padding: 10px;
    width: 45%;
}
.right-wrapper {
    padding: 10px;
    width: 200px;
}
}
@media screen and (max-width: 767px) {
.main-wrapper {
    width: 100%;
}
.left-wrapper {
    padding: 0;
    width: 100%;
}
.home-pageslidercontainer-middle {
    height: 450px;
    width: 100%;
	  padding-bottom: 0;
}
.right-wrapper {
    padding: 0;
    width: 100%;
}
.home-pageslidercontainer-middle {
    max-width: none;
    width: 100%;
	border:0 none;
}
.home-pageslidercontainer-left {
    clear: both;
    float: none;
    margin: auto;
   
    width:100%;
}
.home-page-slider-right{
  clear: both;
    float: none;
    margin: auto;
   
    width:100%;
}
.home-pageslidercontainer-middle img{width:100%}
}
@media screen and (max-width: 479px) {

.home-pageslider{margin-top:0;}

.logofixed{  margin-right: 25px;
    width: 65%;
	}
	.header-text {
    font-size: 26px;
    margin-top: 20px !important;
}
}
</style>

<?php
/*
* Template Name: Carousel

*/
?>
<?php get_header(); ?>
<section class="home-pageslider background-shadow">

<?php
            // get all the categories from the database
            //$cats = get_categories(); 
				
                // loop through the categries
              //  foreach ($cats as $cat) {
					
					
                    // setup the cateogory ID
                   // $cat_id= $cat->term_id;
                    // Make a header for the cateogry
                    //echo "<div class=$class><h2 style=color:$color>".$cat->name."</h2>";
                    // create a custom wordpress query
					//$wp_query = null; $wp_query = $temp;
					//remove_filter('post_limits', 'my_post_limit'); 
                    query_posts('showposts=5');
                    // start the wordpress loop!
					$i=1;
                    if (have_posts()) : while (have_posts()) : the_post(); ?>
						<?php $title='right-left-title'; ?>
						<?php
							if($i==1)
							{
								$color='red';
								$class='home-pageslidercontainer-middle';
								$title='middle-title';
							}
							if($i==2 || $i==3)
							{
								$color='teal';
								$class='home-pageslidercontainer-left';
							}
							if($i==4 || $i==5)
							{
								$color='green';
								$class='home-page-slider-right';
							}
							if($i==1)
							{
								echo "<div class='main-wrapper'>";
							}
							if($i==2)
							{
								echo "<div class='left-wrapper'>";
							}
							if($i==4)
							{
								echo "<div class='right-wrapper'>";
							}
						?>
						<?php echo "<div class=$class>"; ?>
						<?php if (has_post_thumbnail( $post->ID ) ): ?>
							<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
							<!-- <div id="custom-bg" style="background-image: url('<?php echo $image[0]; ?>')"> -->
							<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
								<img src='<?php echo $image[0]; ?>' />
							<!-- </div> -->
							</a>
							<?php endif; ?>
						
                        <?php // create our link now that the post is setup ?>
						<?php if($i==1) { echo "<div class='middle-block-content'>"; }  ?>
                        <a href="<?php the_permalink();?>"><?php  echo "<h1 class=$title>";  ?><?php the_title(); ?><?php echo "</h1>"; ?></h1></a>
						<?php if($i==1) {
							the_excerpt(__('(more…)')); } ?>
						<?php if($i==1) { echo "</div>" ;} ?>
                        <?php echo '</div>'; ?>
						<?php $i++; ?>
						<?php if($i==4)
						{
							echo "</div>";
							echo "</div>";
						} ?>
						<?php if($i==6)
						{
							echo "</div>";
						} ?>
                    <?php endwhile; endif; // done our wordpress loop. Will start again for each category ?>

                <?php// } // done the foreach statement ?>

</section>

<div class="background-shadow clearfix clear">
<div id="primary" class="content-area">
				<div id="content" class="site-content" role="main">
				<?php //query_posts('offset=5'); ?>
				<?php add_filter('post_limits', 'my_post_limit'); ?>
				<?php
					global $myOffset;
					$myOffset = 5;
					$temp = $wp_query;
					$wp_query= null;
					$wp_query = new WP_Query();
					$wp_query->query('offset='.$myOffset.'&showposts=10'.'&paged='.$paged);
					?>
				<?php if ( have_posts() ) : ?>

					<?php while ( have_posts() ) : $wp_query->the_post(); ?>

						<?php
							/* Include the Post-Format-specific template for the content.
							 * If you want to overload this in a child theme then include a file
							 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 */
							get_template_part( 'content', get_post_format() );
						?>

					<?php endwhile; ?>

					<?php athemes_content_nav( 'nav-below' ); ?>
				<?php else : ?>

					<?php get_template_part( 'no-results', 'index' ); ?>

				<?php endif; ?>

				<!-- #content --></div>
			<!-- #primary --></div>


<?php get_sidebar(); ?></div>
<?php get_footer(); ?>

