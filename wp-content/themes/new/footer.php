<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package aThemes
 */
?>
		</div>
	<!-- #main --></div>

	<?php
		/* A sidebar in the footer? Yep. You can can customize
		 * your footer with up to four columns of widgets.
		 */
		get_sidebar( 'footer' );
	?>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="clearfix container">
			<div class="site-info">
				&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?>. All rights reserved.
			</div><!-- .site-info -->

			<div class="site-credit">
<a href="http://www.everything5pounds.com" target="_blank">Everything5pounds.com</a></div><!-- .site-credit -->
		</div>
	<!-- #colophon --></footer>

<?php wp_footer(); ?>
<script>
	jQuery(document).ready(function($){
		$(".posted-on").after('<span class="posted-block-auth"><a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>">By <?php the_author(); ?></a></span>');
		<?php
        $category_detail=get_the_category($post->ID);
            foreach($category_detail as $cd){
            $cat_name= $cd->cat_name;
            $category_id = get_cat_ID( $cd->cat_name );
            $cat_link =get_category_link( $category_id );
            //echo $cd->cat_url;
            } ?>

		$(".posted-block-auth").after('<span class="posted-block-cat"><a href="<?php echo $cat_link; ?>""><?php echo $cat_name;  ?></a></span>');
	});
</script>
<script>
jQuery(document).ready(function(){
	jQuery('.really_simple_share').insertAfter('.entry-title');
});
</script>
</body>
</html>